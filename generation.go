package pbcGo

/*
#include <stdint.h>
#include <pbc/pbc.h>

typedef struct {
	int typeD;
	pbc_param_ptr params;
	uint32_t rbits;
	uint32_t qbits;
} check_pairing_settings_t;

int checkPairing(pbc_cm_t cm, void* p) {
	check_pairing_settings_t* settings = (check_pairing_settings_t*)p;

	return 1;
}
*/
import "C"

// GenerateA generates a pairing on the curve y^2 = x^3 + x over the field F_q
// for some prime q = 3 mod 4. Type A pairings are symmetric (i.e., G1 == G2).
// Type A pairings are best used when speed of computation is the primary
// concern.
//
// To be secure, generic discrete log algorithms must be infeasible in groups of
// order r, and finite field discrete log algorithms must be infeasible in
// groups of order q^2.
//
// For example:
// 	params := pbc.GenerateA(160, 512)
//
// More details: https://crypto.stanford.edu/pbc/manual/ch08s03.html
func GenerateA(rbits uint32, qbits uint32) *Params {
	params := makeParams()
	C.pbc_param_init_a_gen(params.cptr, C.int(rbits), C.int(qbits))
	return params
}
