package pbcGo

/*
#include <pbc/pbc.h>
*/
import "C"

import (
	cryptorand "crypto/rand"
	"io"
	"math/big"
	"math/rand"
)

// RandomSource generates random numbers for consumption by PBC. Rand returns a
// random integer in [0,limit).
type RandomSource interface {
	Rand(limit *big.Int) *big.Int
}

var randomProvider RandomSource

// SetRandomProvider sets the random number source for use by PBC. If provider
// is nil, then PBC will use its internal random number generator, which is the
// default mode. If provider is non-nil, then requests for random numbers will
// be serviced by Go instead of the internal C functions. This is slower, but
// provides greater control. Several convenience functions are provided to set
// common sources of random numbers.
func SetRandomProvider(provider RandomSource) {
	randomProvider = provider
}

type readerProvider struct {
	source io.Reader
}

func (provider readerProvider) Rand(limit *big.Int) (result *big.Int) {
	result, err := cryptorand.Int(provider.source, limit)
	if err != nil {
		panic(ErrEntropyFailure)
	}
	return
}

type randProvider struct {
	source *rand.Rand
}

func (provider randProvider) Rand(limit *big.Int) (result *big.Int) {
	result = &big.Int{}
	result.Rand(provider.source, limit)
	return
}

// SetDefaultRandom causes PBC to use its internal source of random numbers.
// This is the default mode of operation. Internally, PBC will attempt to read
// from /dev/urandom if it exists, or from the Microsoft Crypto API on Windows.
// If neither of these sources is available, the library will fall back to an
// insecure PRNG.
func SetDefaultRandom() { SetRandomProvider(nil) }

func init() {
	SetDefaultRandom()
}
