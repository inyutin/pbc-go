# Introduction
This is a library I edit for purpose of Golang Pairing Cryptography Implementation task  
It based on https://github.com/Nik-U/pbc  
All code is documented by comments

# Dependencies
    PBC - https://github.com/blynn/pbc
    GMP - https://github.com/lattera/glibc
    
# Tests
To run test: `go test .`