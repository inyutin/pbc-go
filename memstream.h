#include <stdio.h>

// memstream_s is a structure that provides platform-independent conversion from
// file descriptor writes to strings
typedef struct memstream_s memstream_t;

// pbc_open_memstream returns a memstream that can be used for writing data
memstream_t* pbc_open_memstream();

// pbc_memstream_to_fd retrieves the file descriptor for a memstream
FILE* pbc_memstream_to_fd(memstream_t* m);

// pbc_close_memstream closes the memstream and returns the written data
int pbc_close_memstream(memstream_t* m, char** bufp, size_t* sizep);
