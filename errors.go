package pbcGo

import "errors"

var (
	ErrInvalidParamString = errors.New("invalid pairing parameters")
	ErrUnknownField       = errors.New("unchecked element initialized in unknown field")
	ErrIllegalOp          = errors.New("operation is illegal for elements of this type")
	ErrUncheckedOp        = errors.New("unchecked element passed to checked operation")
	ErrIncompatible       = errors.New("elements are from incompatible fields or pairings")
	ErrBadPairList        = errors.New("pairing product list is in an invalid format")
	ErrOutOfRange         = errors.New("index out of range")
	ErrEntropyFailure     = errors.New("error while reading from entropy source")
	ErrHashFailure        = errors.New("error while hashing data")
	ErrInternal           = errors.New("a severe internal error has lead to possible memory corruption")
)
