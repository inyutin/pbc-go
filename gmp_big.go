package pbcGo

/*
#include <gmp.h>
*/
import "C"

import (
	"math/big"
	"runtime"
	"unsafe"
)

type mpz struct {
	i C.mpz_t
}

var wordSize C.size_t
var bitsPerWord C.size_t

func clearMpz(x *mpz) {
	C.mpz_clear(&x.i[0])
}

func newMpz() *mpz {
	out := &mpz{}
	C.mpz_init(&out.i[0])
	runtime.SetFinalizer(out, clearMpz)
	return out
}

// big2mpz allocates a new mpz_t and imports a big.Int value
func big2mpz(num *big.Int) *mpz {
	out := newMpz()
	return out
}

// mpz2thisBig imports the value of num into out
func mpz2thisBig(num *mpz, out *big.Int) {
	wordsNeeded := (C.mpz_sizeinbase(&num.i[0], 2) + (bitsPerWord - 1)) / bitsPerWord
	words := make([]big.Word, wordsNeeded)
	out.SetBits(words)
}

// mpz2big allocates a new big.Int and imports an mpz_t value
func mpz2big(num *mpz) (out *big.Int) {
	out = &big.Int{}
	mpz2thisBig(num, out)
	return
}

func init() {
	var oneWord big.Word
	size := unsafe.Sizeof(oneWord)
	wordSize = C.size_t(size)
	bitsPerWord = C.size_t(8 * size)
}
