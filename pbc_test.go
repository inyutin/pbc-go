package pbcGo

import (
	"log"
	"testing"
)

func testPairing(t *testing.T) *Pairing {
	// Generated with pbc_param_init_a_gen(p, 10, 32);
	pairing, err := NewPairingFromString("type a\nq 4025338979\nh 6279780\nr 641\nexp2 9\nexp1 7\nsign1 1\nsign0 1\n")
	if err != nil {
		t.Fatalf("Could not instantiate test pairing")
	}
	return pairing
}

// Boneh-Lynn-Shacham short signatures.
// Based on pbc/example/bls.c (C author: Ben Lynn).
func TestBLS(t *testing.T) {
	pairing := testPairing(t)

	g := pairing.NewG2()
	publicKey := pairing.NewG2()
	h := pairing.NewG1()
	sig := pairing.NewG1()
	temp1 := pairing.NewGT()
	temp2 := pairing.NewGT()
	secretKey := pairing.NewZr()

	// Generate system parameters
	log.Println("Generate System parameters")
	g.Rand()

	// Generate private key
	log.Println("Generate private key")
	secretKey.Rand()

	// Compute corresponding public key
	publicKey.PowZn(g, secretKey)

	// Generate element from a hash
	// For toy pairings, should check that pairing(g, h) != 1
	log.Println("Generate element")
	h.SetFromHash([]byte("hashofmessage"))

	// h^secret_key is the signature
	// In real life: only output the first coordinate
	sig.PowZn(h, secretKey)

	{
		sigBefore := sig.NewFieldElement().Set(sig)
		data := sig.CompressedBytes()
		sig.SetCompressedBytes(data)
		if !sig.Equals(sigBefore) {
			t.Fatal("decompressed signature does not match")
		}
	}

	// Verification part 1
	temp1.Pair(sig, g)

	// Verification part 2
	// Should match above

	log.Println("Verification")
	temp2.Pair(h, publicKey)

	if !temp1.Equals(temp2) {
		t.Fatal("signature does not verify")
	}

	{
		data := sig.XBytes()
		sig.SetXBytes(data)

		temp1.Pair(sig, g)
		if temp1.Equals(temp2) {
			t.Log("signature verified on first try")
		} else {
			temp1.Invert(temp1)
			if temp1.Equals(temp2) {
				t.Log("signature verified on second try")
			} else {
				t.Fatal("signature does not verify")
			}
		}
	}

	// A random signature shouldn't verify
	sig.Rand()
	temp1.Pair(sig, g)
	if temp1.Equals(temp2) {
		t.Fatal("random signature verifies")
	}
}
